import React, { Component } from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap-grid.min.css';

import Homelog from '../src/components/Homelog';
import Login from '../src/components/Login';
import configFire from './configFire';
import Usuarios from './components/Usuarios';
import Registro from './components/Registro';



class  App extends Component{

    constructor(props){
        super(props);
        this.state = {
            user: null,
        }
        this.authListener = this.authListener.bind(this);

    }

    componentDidMount(){
        this.authListener();
    }

    authListener(){
        configFire.auth().onAuthStateChanged((user) => {
            if(user ){
                this.setState({ user });
            }else{
                this.setState({ user: null});
            }
        })

    }
    render(){
    return ( 
        <div >
        {this.state.user ? (<Registro/>) : (<Usuarios/> )}
        {this.state.user ? (<Usuarios/>) : (<Login/>)}

        </div>
    );
}
}

export default App;