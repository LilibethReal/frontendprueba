import React from 'react';
import {NavLink } from 'react-router-dom';

const Nav = () => (
    <nav>
        <ul className='lista'>
            <li >
                <NavLink to="/inicio">Iniciar Sesión</NavLink>
            </li>
            <li>
                <NavLink to="/registro">Resgistra tu cuenta</NavLink>
            </li>
            <li>
                <NavLink to="/cerrarsesion">Cerrar Sesión</NavLink>
            </li>
            <li>
                <NavLink to="/usuarios">Usuarios</NavLink>
            </li>

        </ul>
    </nav>
)

export default Nav;