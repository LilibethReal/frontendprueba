import React from 'react';
import configFire from '../configFire'

class Homelog extends React.Component{

    logout(){
        configFire.auth().signOut();

    }
    render(){
        return(
            <div>
                <h1>Tu estas logeado</h1>
                <button className="submit"  type="button" onClick={this.logout}>Cerrar Sesión</button>
            </div>
        
        )

            
    }
}
export default Homelog;