import React , {Component}from 'react';
import social from '../social';
import configFire from '../configFire';


 class Login extends React.Component {

    login(){
        const email = document.querySelector("#email").value;
        const password = document.querySelector("#password").value;
        configFire.auth().signInWithEmailAndPassword(email, password)
            .then((u) => {
            console.log("Iniciado");
            })
            .catch((err) => {
                console.log("error" + err.toString());
            })
        }

    registrarse(){
        const email = document.querySelector("#email").value;
        const password = document.querySelector("#password").value;
        configFire.auth().createUserWithEmailAndPassword(email, password)
    .then((u) => {
        console.log("Registrado");
    })
    .catch((err) => {
        console.log("error" + err.toString());
   })
}
    render(){
    return(

    <div className="cont">
            <div className="contenedor">
            <div className="form sign-in">
                <h2>Iniciar Sesión</h2>
                <label>
                    <span>Dirección de correo</span>
                    <input type="email" name="correo" placeholder="Escribe tu correo" id="email"></input>
                </label>
                <label>
                    <span>Contraseña</span>
                    <input type="password" name="contraseña" placeholder="Escribe tu contraseña" id="password"></input>
                </label>
                <button className="submit"  type="button" onClick={this.registrarse}>Registrarse </button>
                <button className="submit"  type="button" onClick={this.login}>Inicia sesion</button>
               <p className="forgot-pass">Olvidé mi contraseña</p>

               <div className="red-social">
                   <ul>
                       <li><img src={social.img1} alt="" ></img></li>
                       <li><img src={social.img2} alt=""></img></li>
                       <li><img src={social.img3} alt=""></img></li>
                   </ul>
               </div>
            </div>

            <div className="sub-cont">
                <div className="img">
                    <div className="img-text m-up">
                        <h2>¿Eres usuario nuevo?</h2>
                        <p>Regístrate y descubre los beneficios </p>
                    </div>
                    <div className="img-text m-in">
                        <h2>¿Eres usuario?</h2>
                        <p>Si ya eres parte de nosotros inicia sesión </p>
                    </div>
                    
                </div>

            </div>
            </div>
        </div>
  );
 }
}
export default Login;