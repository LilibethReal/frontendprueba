import React, {useState, useEffect } from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import { db} from '../configFire';
import { useConfig } from '../useConfig';
import configFire from '../configFire';



 //consiguracion del consumo de la api

function Usuarios (){

    useEffect(function () {
        console.log('render!')
    })

    const [url, setUrl]= useState('https://rickandmortyapi.com/api/character/?name=')
    const [info, setInfo] = useState({})
    const [ results, setResults] = useState([])
    const [ search, setSearch] = useState("")
    const { loading, messages, error} = useConfig();
    const [ message, setMessages] = React.useState('');


    const sendMessage = (e) => {
        e.preventDefault();
        db.collection('messages').add({timestamp: Date.now(), message });

    }

    useEffect(() => {
        console.log('url:', url);
        console.log('info:', info);
        console.log('results:', results);
        console.log('search:', search);
    }, [url, info,results, search]);

    useEffect(() => {
        axios.get(`${url}${search}`)
        .then((result) => {
           // console.log(result)
            setInfo(result.data.info)
            setResults(result.data.results)
        })
        .catch((error) => {
            console.log(error)
        })
    }, [search])


    
   // useEffect(() => {
//    setResults("esto es un test")
  //  }, [url, info, results])
  configFire.auth().signOut().then(function() {
    // Sign-out successful.
  }).catch(function(error) {
    // An error happened.
  });
    return(
        <>   
        
        <button  className="submit" type="button"  >Cerrar Sesión</button>

        <h2>Busca tu personaje</h2>
            <input onChange={(e) =>{
                setSearch(e.target.value)
            }} 
            value={search}
            type="text"/>
       
<table  className="table  table-borderless table-success table-hover table-striped table-sm">
{results.map((result, index)=> (
    
    <thead key={index}>
        <tbody className="ws-frames-list-body">

        </tbody>
        <tr className="table-active">
        <img src={result.image} alt={`foto of ${result.name}`} />

            <th>Nombre:{result.name}<br></br>Especie:{result.species}<br></br>Estado: {result.status}<br></br>
            Estado: {result.status}<br></br>Género: {result.gender}</th>
            <h2>MI CUENTA</h2>

            <select class="form-select" aria-label="Default select example">
            <option selected>Abre y selecciona la operación</option>
            <option value="1">Enviar dinero</option>
            <option value="2">Pagar préstamo</option>
            <option value="3">Ingresar dinero</option>
            <option value="4">Pedir préstamo al banco</option>
            <option value="5">Pedir préstamo a un usuario</option>
            </select>
            

            <div className="form-group">
                <select name="results" className="form-control">
                {results.map(elemento =>(
                    <option >{elemento.name}</option>
                ))}
                </select>
            </div>

            <div class="input-group">
            <input value={message} onChange={(e) => setMessages(e.target.value)}
            type="text" class="form-control" aria-label="Dollar amount (with dot and two decimal places)"/>
            <span class="input-group-text">€</span>
            <span class="input-group-text">0.00</span>
            <ul>
                {messages.map(m => <li  key={m.id} class="input-group-text">{m.message}</li>)}

            </ul>
            </div>


            <button type="submit" onClick={sendMessage} class="btn btn-dark btn-sm">Aceptar</button><br></br><br></br>     
        </tr>
    </thead>
     ))}
    
</table>   
 </>

  );
                }
export default Usuarios;